# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import health
from . import health_stock
from . import shipment


def register():
    Pool.register(
        health.PatientPrescriptionOrder,
        health.Medicament,
        health.HealthInstitution,
        health.PrescriptionLine,
        health_stock.CreatePrescriptionAndStockMoveStart,
        health_stock.CreatePrescriptionAndStockMoveStartLine,
        shipment.ShipmentIn,        
        module='health_stock_hesm', type_='model')
    Pool.register(
        health_stock.CreatePrescriptionAndStockMove,
        module='health_stock_hesm', type_='wizard')
