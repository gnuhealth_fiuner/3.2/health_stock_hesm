# -*- coding: utf-8 -*-
# This file is part of health_stock_hesm module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['ShipmentIn']


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']

    party_type = fields.Selection([
        ('laboratory', 'Laboratory'),
        ('supplier', 'Supplier'),
        ('social_aid', 'Social Aid'),
        ('individual', 'Individual'),
        ], 'Party Type', sort=False,
        states=_states, depends=_depends)

    @staticmethod
    def default_party_type():
        return 'supplier'
